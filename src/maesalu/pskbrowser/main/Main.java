package maesalu.pskbrowser.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import maesalu.pskbrowser.browser.Browser;
import maesalu.pskbrowser.graphics.Graphics;

import java.io.IOException;

public class Main extends Application {
    private static Browser browser;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("browser.fxml"));
        Canvas canvas = (Canvas) root.getChildrenUnmodifiable().get(4);
        Graphics.initializeInstance(canvas);
        primaryStage.setTitle("PSK-Browser");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    public static void main(String[] args) throws IOException {
        browser = new Browser();
        launch(args);
    }

    public static Browser getBrowser() {
        return browser;
    }
}
