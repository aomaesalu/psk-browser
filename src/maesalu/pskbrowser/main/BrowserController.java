package maesalu.pskbrowser.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextField;
import maesalu.pskbrowser.exceptions.InvalidIndexException;

import java.io.IOException;
import java.util.List;

/**
 * The class responsible for front end interaction.
 * @author Ants-Oskar Mäesalu
 */
public class BrowserController {
    @FXML
    private static TextField urlField;
    @FXML
    private static Canvas canvas;

    /**
     * Execute actions taking place when the back button is clicked.
     * The user is moved back in the current tab's site history list.
     * @param event
     */
    public void handleBackButtonAction(ActionEvent event) {
        try {
            Main.getBrowser().getCurrentTab().back();
        } catch (InvalidIndexException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Execute actions taking place when the forward button is clicked.
     * The user is moved forward in the current tab's site history list.
     * @param event
     */
    public void handleForwardButtonAction(ActionEvent event) {
        try {
            Main.getBrowser().getCurrentTab().forward();
        } catch (InvalidIndexException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Execute actions taking place when "enter" is pressed while the URL field is active.
     * The user will be directed to the site.
     * @param event
     */
    public void handleURLAction(ActionEvent event) {
        try {
            Main.getBrowser().getCurrentTab().visitSite(urlField.getText());
        } catch (InvalidIndexException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}