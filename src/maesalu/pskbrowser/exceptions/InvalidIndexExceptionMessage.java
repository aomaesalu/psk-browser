package maesalu.pskbrowser.exceptions;

/**
 * Enumerator for messages of the exceptions caused by an invalid index.
 * @author Ants-Oskar Mäesalu
 */
public enum InvalidIndexExceptionMessage {
    DEFAULT("You referred to an invalid index"),
    TAB("The tab you referred to does not exist."),
    SITE("The site you referred to in the history does not exist.");

    private String message;

    /**
     * Private constructor.
     * @param message
     */
    private InvalidIndexExceptionMessage(String message) {
        this.message = message;
    }

    /**
     * Return the exception message.
     * @return
     */
    public String getMessage() {
        return message;
    }
}
