package maesalu.pskbrowser.exceptions;

/**
 * Exception class for situations when the user refers to an invalid index in a list.
 * @author Ants-Oskar Mäesalu
 */
public class InvalidIndexException extends Throwable {
    /**
     * Constructor without a message. A default message is used.
     */
    public InvalidIndexException() {
        super(InvalidIndexExceptionMessage.DEFAULT.getMessage());
    }

    /**
     * Constructor with a string type message.
     * @param message
     */
    public InvalidIndexException(String message) {
        super(message);
    }

    /**
     * Constructor with an enumerator type message.
     * @param message
     */
    public InvalidIndexException(InvalidIndexExceptionMessage message) {
        super(message.getMessage());
    }
}
