package maesalu.pskbrowser.languages.html.data;

/**
 * The class holding HTML attribute data.
 * @author Ants-Oskar Mäesalu
 */
public class Attribute {
    String name;
    String value;

    /**
     * Main constructor with the attribute's name and value.
     * @param name
     * @param value
     */
    public Attribute(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Return the attribute's name.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Return the attribute's value.
     * @return
     */
    public String getValue() {
        return value;
    }
}
