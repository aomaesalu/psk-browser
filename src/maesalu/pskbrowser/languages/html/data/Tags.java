package maesalu.pskbrowser.languages.html.data;

public enum Tags {
    HTML("html"),
    HEAD("head"),
    BODY("body"),
    PARAGRAPH("p");

    private String tag;

    /**
     * Private constructor.
     * @param tag
     */
    private Tags(String tag) {
        this.tag = tag;
    }

    /**
     * Return the tag name.
     * @return
     */
    public String getTag() {
        return tag;
    }
}
