package maesalu.pskbrowser.languages.html.data;

import java.util.ArrayList;
import java.util.List;

/**
 * The class holding HTML tag data.
 * @author Ants-Oskar Mäesalu
 */
public class Tag {
    String name;
    List<Attribute> attributes;
    List<Tag> tags;
    String contents;

    /**
     * Constructor with tag name and a list of attributes of this tag.
     * @param name
     * @param attributes
     */
    public Tag(String name, List<Attribute> attributes) {
        this.name = name;
        this.attributes = attributes;
        this.tags = new ArrayList<Tag>();
    }

    /**
     * Add a new HTML tag to the child tag list.
     * @param newTag
     */
    public void addChildTag(Tag newTag) {
        tags.add(newTag);
    }

    /**
     * Set new plaintext contents to the HTML tag.
     * @param contents
     */
    public void setContents(String contents) {
        this.contents = contents;
    }

    /**
     * Return the HTML tag's name.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Return the HTML tag's attribute list.
     * @return
     */
    public List<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * Return the list of child HTML tags.
     * @return
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Return if the tag has any child tags.
     * @return
     */
    public boolean hasChildTags() {
        return !tags.isEmpty();
    }

    /**
     * Return the number of child tags in this tag.
     * @return
     */
    public Integer getNumberOfChildTags() {
        return tags.size();
    }

    /**
     * Return the child tag with the specified index.
     * @param i
     * @return
     */
    public Tag getChildTag(Integer index) {
        return tags.get(index);
    }

    /**
     * Return the plain text contents of the HTML tag.
     * @return
     */
    public String getContents() {
        return contents;
    }
}
