package maesalu.pskbrowser.languages.html;

import maesalu.pskbrowser.graphics.Graphics;
import maesalu.pskbrowser.languages.html.data.Tag;
import maesalu.pskbrowser.languages.html.data.Tags;

import java.util.List;

/**
 * Class responsible for depicting the parsed HTML on the browser's screen.
 * @author Ants-Oskar Mäesalu
 */
public class HTMLDepicter {

    /**
     * Constructor with the HTML tags' tree as a parameter.
     * @param tags
     */
    public HTMLDepicter(List<Tag> tags) {
        depict(tags);
    }

    /**
     * Depict the tags list.
     * @param tags
     */
    public void depict(List<Tag> tags) {
        for (int i = 0; i < tags.size(); i++) {
            if (tags.get(i) != null) {
                depict(tags.get(i));
            }
        }
    }

    /**
     * Depict a single tag.
     * @param tag
     */
    public void depict(Tag tag) {
        if (tag.hasChildTags()) {
            for (int i = 0; i < tag.getNumberOfChildTags(); i++) {
                depict(tag.getChildTag(i));
            }
        } else {
            if (tag.getName().toLowerCase().equals(Tags.PARAGRAPH.getTag())) {
                Graphics.getInstance().newParagraph(tag.getContents());
            }
        }
    }
}
