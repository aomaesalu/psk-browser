package maesalu.pskbrowser.languages.html.gen;// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/psk-browser/src/maesalu/pskbrowser/languages/html\HTML.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link HTMLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface HTMLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link HTMLParser#attributeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeName(@NotNull HTMLParser.AttributeNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlElement(@NotNull HTMLParser.HtmlElementContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlStartTag}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlStartTag(@NotNull HTMLParser.HtmlStartTagContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#docType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDocType(@NotNull HTMLParser.DocTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#attributeValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeValue(@NotNull HTMLParser.AttributeValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlEndTag}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlEndTag(@NotNull HTMLParser.HtmlEndTagContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#tagName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTagName(@NotNull HTMLParser.TagNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#html}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtml(@NotNull HTMLParser.HtmlContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#tagAttribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTagAttribute(@NotNull HTMLParser.TagAttributeContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#plainText}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlainText(@NotNull HTMLParser.PlainTextContext ctx);

	/**
	 * Visit a parse tree produced by {@link HTMLParser#htmlContent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlContent(@NotNull HTMLParser.HtmlContentContext ctx);
}