package maesalu.pskbrowser.languages.html.gen;// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/psk-browser/src/maesalu/pskbrowser/languages/html\HTML.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HTMLParser}.
 */
public interface HTMLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HTMLParser#attributeName}.
	 * @param ctx the parse tree
	 */
	void enterAttributeName(@NotNull HTMLParser.AttributeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#attributeName}.
	 * @param ctx the parse tree
	 */
	void exitAttributeName(@NotNull HTMLParser.AttributeNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlElement}.
	 * @param ctx the parse tree
	 */
	void enterHtmlElement(@NotNull HTMLParser.HtmlElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlElement}.
	 * @param ctx the parse tree
	 */
	void exitHtmlElement(@NotNull HTMLParser.HtmlElementContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlStartTag}.
	 * @param ctx the parse tree
	 */
	void enterHtmlStartTag(@NotNull HTMLParser.HtmlStartTagContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlStartTag}.
	 * @param ctx the parse tree
	 */
	void exitHtmlStartTag(@NotNull HTMLParser.HtmlStartTagContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#docType}.
	 * @param ctx the parse tree
	 */
	void enterDocType(@NotNull HTMLParser.DocTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#docType}.
	 * @param ctx the parse tree
	 */
	void exitDocType(@NotNull HTMLParser.DocTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#attributeValue}.
	 * @param ctx the parse tree
	 */
	void enterAttributeValue(@NotNull HTMLParser.AttributeValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#attributeValue}.
	 * @param ctx the parse tree
	 */
	void exitAttributeValue(@NotNull HTMLParser.AttributeValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlEndTag}.
	 * @param ctx the parse tree
	 */
	void enterHtmlEndTag(@NotNull HTMLParser.HtmlEndTagContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlEndTag}.
	 * @param ctx the parse tree
	 */
	void exitHtmlEndTag(@NotNull HTMLParser.HtmlEndTagContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#tagName}.
	 * @param ctx the parse tree
	 */
	void enterTagName(@NotNull HTMLParser.TagNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#tagName}.
	 * @param ctx the parse tree
	 */
	void exitTagName(@NotNull HTMLParser.TagNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#html}.
	 * @param ctx the parse tree
	 */
	void enterHtml(@NotNull HTMLParser.HtmlContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#html}.
	 * @param ctx the parse tree
	 */
	void exitHtml(@NotNull HTMLParser.HtmlContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#tagAttribute}.
	 * @param ctx the parse tree
	 */
	void enterTagAttribute(@NotNull HTMLParser.TagAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#tagAttribute}.
	 * @param ctx the parse tree
	 */
	void exitTagAttribute(@NotNull HTMLParser.TagAttributeContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#plainText}.
	 * @param ctx the parse tree
	 */
	void enterPlainText(@NotNull HTMLParser.PlainTextContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#plainText}.
	 * @param ctx the parse tree
	 */
	void exitPlainText(@NotNull HTMLParser.PlainTextContext ctx);

	/**
	 * Enter a parse tree produced by {@link HTMLParser#htmlContent}.
	 * @param ctx the parse tree
	 */
	void enterHtmlContent(@NotNull HTMLParser.HtmlContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link HTMLParser#htmlContent}.
	 * @param ctx the parse tree
	 */
	void exitHtmlContent(@NotNull HTMLParser.HtmlContentContext ctx);
}