package maesalu.pskbrowser.languages.html.gen;// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/psk-browser/src/maesalu/pskbrowser/languages/html\HTML.g4 by ANTLR 4.x

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HTMLParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TagStart=1, TagStartSlash=2, TagStartBang=3, TagEnd=4, AlphabetLetter=5, 
		ForeignLetter=6, Digit=7, SpecialSymbol=8, EqualSign=9, DoubleQuote=10, 
		Space=11, Whitespace=12;
	public static final String[] tokenNames = {
		"<INVALID>", "'<'", "'</'", "'<!'", "'>'", "AlphabetLetter", "ForeignLetter", 
		"Digit", "SpecialSymbol", "'='", "'\"'", "Space", "Whitespace"
	};
	public static final int
		RULE_html = 0, RULE_htmlContent = 1, RULE_htmlElement = 2, RULE_docType = 3, 
		RULE_htmlStartTag = 4, RULE_tagAttribute = 5, RULE_htmlEndTag = 6, RULE_tagName = 7, 
		RULE_attributeName = 8, RULE_attributeValue = 9, RULE_plainText = 10;
	public static final String[] ruleNames = {
		"html", "htmlContent", "htmlElement", "docType", "htmlStartTag", "tagAttribute", 
		"htmlEndTag", "tagName", "attributeName", "attributeValue", "plainText"
	};

	@Override
	public String getGrammarFileName() { return "HTML.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public HTMLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class HtmlContext extends ParserRuleContext {
		public HtmlContentContext htmlContent(int i) {
			return getRuleContext(HtmlContentContext.class,i);
		}
		public List<HtmlContentContext> htmlContent() {
			return getRuleContexts(HtmlContentContext.class);
		}
		public HtmlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_html; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterHtml(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitHtml(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitHtml(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlContext html() throws RecognitionException {
		HtmlContext _localctx = new HtmlContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_html);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TagStart || _la==TagStartBang) {
				{
				{
				setState(22); htmlContent();
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HtmlContentContext extends ParserRuleContext {
		public HtmlElementContext htmlElement() {
			return getRuleContext(HtmlElementContext.class,0);
		}
		public DocTypeContext docType() {
			return getRuleContext(DocTypeContext.class,0);
		}
		public HtmlContentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_htmlContent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterHtmlContent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitHtmlContent(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitHtmlContent(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlContentContext htmlContent() throws RecognitionException {
		HtmlContentContext _localctx = new HtmlContentContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_htmlContent);
		try {
			setState(30);
			switch (_input.LA(1)) {
			case TagStartBang:
				enterOuterAlt(_localctx, 1);
				{
				setState(28); docType();
				}
				break;
			case TagStart:
				enterOuterAlt(_localctx, 2);
				{
				setState(29); htmlElement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HtmlElementContext extends ParserRuleContext {
		public HtmlElementContext htmlElement(int i) {
			return getRuleContext(HtmlElementContext.class,i);
		}
		public List<HtmlElementContext> htmlElement() {
			return getRuleContexts(HtmlElementContext.class);
		}
		public HtmlEndTagContext htmlEndTag() {
			return getRuleContext(HtmlEndTagContext.class,0);
		}
		public HtmlStartTagContext htmlStartTag() {
			return getRuleContext(HtmlStartTagContext.class,0);
		}
		public PlainTextContext plainText() {
			return getRuleContext(PlainTextContext.class,0);
		}
		public HtmlElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_htmlElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterHtmlElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitHtmlElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitHtmlElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlElementContext htmlElement() throws RecognitionException {
		HtmlElementContext _localctx = new HtmlElementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_htmlElement);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(32); htmlStartTag();
			setState(40);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(36);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(33); htmlElement();
						}
						} 
					}
					setState(38);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				}
				}
				break;

			case 2:
				{
				setState(39); plainText();
				}
				break;
			}
			setState(43);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				setState(42); htmlEndTag();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DocTypeContext extends ParserRuleContext {
		public List<TagNameContext> tagName() {
			return getRuleContexts(TagNameContext.class);
		}
		public TerminalNode TagStartBang() { return getToken(HTMLParser.TagStartBang, 0); }
		public TagNameContext tagName(int i) {
			return getRuleContext(TagNameContext.class,i);
		}
		public TerminalNode Space() { return getToken(HTMLParser.Space, 0); }
		public TerminalNode TagEnd() { return getToken(HTMLParser.TagEnd, 0); }
		public DocTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_docType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterDocType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitDocType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitDocType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DocTypeContext docType() throws RecognitionException {
		DocTypeContext _localctx = new DocTypeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_docType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45); match(TagStartBang);
			setState(46); tagName();
			setState(47); match(Space);
			setState(48); tagName();
			setState(49); match(TagEnd);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HtmlStartTagContext extends ParserRuleContext {
		public TagNameContext tagName() {
			return getRuleContext(TagNameContext.class,0);
		}
		public TerminalNode TagStart() { return getToken(HTMLParser.TagStart, 0); }
		public TagAttributeContext tagAttribute(int i) {
			return getRuleContext(TagAttributeContext.class,i);
		}
		public List<TagAttributeContext> tagAttribute() {
			return getRuleContexts(TagAttributeContext.class);
		}
		public TerminalNode TagEnd() { return getToken(HTMLParser.TagEnd, 0); }
		public HtmlStartTagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_htmlStartTag; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterHtmlStartTag(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitHtmlStartTag(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitHtmlStartTag(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlStartTagContext htmlStartTag() throws RecognitionException {
		HtmlStartTagContext _localctx = new HtmlStartTagContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_htmlStartTag);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51); match(TagStart);
			setState(52); tagName();
			setState(56);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Space) {
				{
				{
				setState(53); tagAttribute();
				}
				}
				setState(58);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(59); match(TagEnd);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TagAttributeContext extends ParserRuleContext {
		public AttributeValueContext attributeValue() {
			return getRuleContext(AttributeValueContext.class,0);
		}
		public TerminalNode EqualSign() { return getToken(HTMLParser.EqualSign, 0); }
		public AttributeNameContext attributeName() {
			return getRuleContext(AttributeNameContext.class,0);
		}
		public TerminalNode DoubleQuote(int i) {
			return getToken(HTMLParser.DoubleQuote, i);
		}
		public TerminalNode Space(int i) {
			return getToken(HTMLParser.Space, i);
		}
		public List<TerminalNode> Space() { return getTokens(HTMLParser.Space); }
		public List<TerminalNode> DoubleQuote() { return getTokens(HTMLParser.DoubleQuote); }
		public TagAttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tagAttribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterTagAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitTagAttribute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitTagAttribute(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TagAttributeContext tagAttribute() throws RecognitionException {
		TagAttributeContext _localctx = new TagAttributeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_tagAttribute);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(61); match(Space);
				}
				}
				setState(64); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Space );
			setState(66); attributeName();
			setState(70);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Space) {
				{
				{
				setState(67); match(Space);
				}
				}
				setState(72);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(73); match(EqualSign);
			setState(77);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Space) {
				{
				{
				setState(74); match(Space);
				}
				}
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(80); match(DoubleQuote);
			setState(81); attributeValue();
			setState(82); match(DoubleQuote);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HtmlEndTagContext extends ParserRuleContext {
		public TerminalNode TagStartSlash() { return getToken(HTMLParser.TagStartSlash, 0); }
		public TagNameContext tagName() {
			return getRuleContext(TagNameContext.class,0);
		}
		public TerminalNode TagEnd() { return getToken(HTMLParser.TagEnd, 0); }
		public HtmlEndTagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_htmlEndTag; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterHtmlEndTag(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitHtmlEndTag(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitHtmlEndTag(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlEndTagContext htmlEndTag() throws RecognitionException {
		HtmlEndTagContext _localctx = new HtmlEndTagContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_htmlEndTag);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84); match(TagStartSlash);
			setState(85); tagName();
			setState(86); match(TagEnd);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TagNameContext extends ParserRuleContext {
		public TerminalNode AlphabetLetter(int i) {
			return getToken(HTMLParser.AlphabetLetter, i);
		}
		public List<TerminalNode> AlphabetLetter() { return getTokens(HTMLParser.AlphabetLetter); }
		public TagNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tagName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterTagName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitTagName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitTagName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TagNameContext tagName() throws RecognitionException {
		TagNameContext _localctx = new TagNameContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_tagName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(88); match(AlphabetLetter);
				}
				}
				setState(91); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==AlphabetLetter );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeNameContext extends ParserRuleContext {
		public TerminalNode AlphabetLetter(int i) {
			return getToken(HTMLParser.AlphabetLetter, i);
		}
		public List<TerminalNode> AlphabetLetter() { return getTokens(HTMLParser.AlphabetLetter); }
		public AttributeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterAttributeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitAttributeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitAttributeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeNameContext attributeName() throws RecognitionException {
		AttributeNameContext _localctx = new AttributeNameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_attributeName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(93); match(AlphabetLetter);
				}
				}
				setState(96); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==AlphabetLetter );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeValueContext extends ParserRuleContext {
		public PlainTextContext plainText() {
			return getRuleContext(PlainTextContext.class,0);
		}
		public AttributeValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterAttributeValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitAttributeValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitAttributeValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeValueContext attributeValue() throws RecognitionException {
		AttributeValueContext _localctx = new AttributeValueContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_attributeValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98); plainText();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlainTextContext extends ParserRuleContext {
		public TerminalNode EqualSign(int i) {
			return getToken(HTMLParser.EqualSign, i);
		}
		public TerminalNode ForeignLetter(int i) {
			return getToken(HTMLParser.ForeignLetter, i);
		}
		public List<TerminalNode> SpecialSymbol() { return getTokens(HTMLParser.SpecialSymbol); }
		public TerminalNode Space(int i) {
			return getToken(HTMLParser.Space, i);
		}
		public TerminalNode AlphabetLetter(int i) {
			return getToken(HTMLParser.AlphabetLetter, i);
		}
		public List<TerminalNode> Space() { return getTokens(HTMLParser.Space); }
		public TerminalNode SpecialSymbol(int i) {
			return getToken(HTMLParser.SpecialSymbol, i);
		}
		public List<TerminalNode> EqualSign() { return getTokens(HTMLParser.EqualSign); }
		public List<TerminalNode> Digit() { return getTokens(HTMLParser.Digit); }
		public List<TerminalNode> ForeignLetter() { return getTokens(HTMLParser.ForeignLetter); }
		public TerminalNode DoubleQuote(int i) {
			return getToken(HTMLParser.DoubleQuote, i);
		}
		public TerminalNode Digit(int i) {
			return getToken(HTMLParser.Digit, i);
		}
		public List<TerminalNode> AlphabetLetter() { return getTokens(HTMLParser.AlphabetLetter); }
		public List<TerminalNode> DoubleQuote() { return getTokens(HTMLParser.DoubleQuote); }
		public PlainTextContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plainText; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).enterPlainText(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HTMLListener) ((HTMLListener)listener).exitPlainText(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HTMLVisitor) return ((HTMLVisitor<? extends T>)visitor).visitPlainText(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlainTextContext plainText() throws RecognitionException {
		PlainTextContext _localctx = new PlainTextContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_plainText);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(103);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(100);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AlphabetLetter) | (1L << ForeignLetter) | (1L << Digit) | (1L << SpecialSymbol) | (1L << EqualSign) | (1L << DoubleQuote) | (1L << Space))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
					} 
				}
				setState(105);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\16m\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\3\3\3\5\3!\n\3\3\4\3\4\7\4%\n"+
		"\4\f\4\16\4(\13\4\3\4\5\4+\n\4\3\4\5\4.\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\6\3\6\3\6\7\69\n\6\f\6\16\6<\13\6\3\6\3\6\3\7\6\7A\n\7\r\7\16\7B\3\7"+
		"\3\7\7\7G\n\7\f\7\16\7J\13\7\3\7\3\7\7\7N\n\7\f\7\16\7Q\13\7\3\7\3\7\3"+
		"\7\3\7\3\b\3\b\3\b\3\b\3\t\6\t\\\n\t\r\t\16\t]\3\n\6\na\n\n\r\n\16\nb"+
		"\3\13\3\13\3\f\7\fh\n\f\f\f\16\fk\13\f\3\f\2\2\r\2\4\6\b\n\f\16\20\22"+
		"\24\26\2\3\3\2\7\rm\2\33\3\2\2\2\4 \3\2\2\2\6\"\3\2\2\2\b/\3\2\2\2\n\65"+
		"\3\2\2\2\f@\3\2\2\2\16V\3\2\2\2\20[\3\2\2\2\22`\3\2\2\2\24d\3\2\2\2\26"+
		"i\3\2\2\2\30\32\5\4\3\2\31\30\3\2\2\2\32\35\3\2\2\2\33\31\3\2\2\2\33\34"+
		"\3\2\2\2\34\3\3\2\2\2\35\33\3\2\2\2\36!\5\b\5\2\37!\5\6\4\2 \36\3\2\2"+
		"\2 \37\3\2\2\2!\5\3\2\2\2\"*\5\n\6\2#%\5\6\4\2$#\3\2\2\2%(\3\2\2\2&$\3"+
		"\2\2\2&\'\3\2\2\2\'+\3\2\2\2(&\3\2\2\2)+\5\26\f\2*&\3\2\2\2*)\3\2\2\2"+
		"+-\3\2\2\2,.\5\16\b\2-,\3\2\2\2-.\3\2\2\2.\7\3\2\2\2/\60\7\5\2\2\60\61"+
		"\5\20\t\2\61\62\7\r\2\2\62\63\5\20\t\2\63\64\7\6\2\2\64\t\3\2\2\2\65\66"+
		"\7\3\2\2\66:\5\20\t\2\679\5\f\7\28\67\3\2\2\29<\3\2\2\2:8\3\2\2\2:;\3"+
		"\2\2\2;=\3\2\2\2<:\3\2\2\2=>\7\6\2\2>\13\3\2\2\2?A\7\r\2\2@?\3\2\2\2A"+
		"B\3\2\2\2B@\3\2\2\2BC\3\2\2\2CD\3\2\2\2DH\5\22\n\2EG\7\r\2\2FE\3\2\2\2"+
		"GJ\3\2\2\2HF\3\2\2\2HI\3\2\2\2IK\3\2\2\2JH\3\2\2\2KO\7\13\2\2LN\7\r\2"+
		"\2ML\3\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2\2RS\7\f\2"+
		"\2ST\5\24\13\2TU\7\f\2\2U\r\3\2\2\2VW\7\4\2\2WX\5\20\t\2XY\7\6\2\2Y\17"+
		"\3\2\2\2Z\\\7\7\2\2[Z\3\2\2\2\\]\3\2\2\2][\3\2\2\2]^\3\2\2\2^\21\3\2\2"+
		"\2_a\7\7\2\2`_\3\2\2\2ab\3\2\2\2b`\3\2\2\2bc\3\2\2\2c\23\3\2\2\2de\5\26"+
		"\f\2e\25\3\2\2\2fh\t\2\2\2gf\3\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2j\27"+
		"\3\2\2\2ki\3\2\2\2\16\33 &*-:BHO]bi";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}