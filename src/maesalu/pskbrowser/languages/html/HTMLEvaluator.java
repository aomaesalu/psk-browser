package maesalu.pskbrowser.languages.html;


import maesalu.pskbrowser.languages.html.data.Attribute;
import maesalu.pskbrowser.languages.html.data.Tag;
import maesalu.pskbrowser.languages.html.gen.HTMLLexer;
import maesalu.pskbrowser.languages.html.gen.HTMLParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Class responsible for evaluating the HTML parse tree contents.
 * @author Ants-Oskar Mäesalu
 */
public class HTMLEvaluator {
    private List<Tag> tags;

    /**
     * Constructor with the source code's input stream as a parameter.
     * @param input
     * @throws IOException
     */
    public HTMLEvaluator(InputStream input) throws IOException {
        ANTLRInputStream inputStream = new ANTLRInputStream(input);
        HTMLLexer lexer = new HTMLLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        HTMLParser parser = new HTMLParser(tokens);
        ParseTree parseTree = parser.html();
        tags = evaluate(parseTree);
    }

    /**
     * Evaluate the HTML source code.
     * @param parseTree
     * @return
     */
    private List<Tag> evaluate(ParseTree parseTree) {
        List<Tag> tags = new ArrayList<Tag>();
        if (parseTree instanceof HTMLParser.HtmlContext) {
            for (int i = 0; i < parseTree.getChildCount(); i++) {
                tags.add(evaluateHtmlContent(parseTree.getChild(i)));
            }
        } else {
            throw new InputMismatchException();
        }
        return tags;
    }

    /**
     * Evaluate a single HTML master entity.
     * @param parseTree
     * @return
     */
    private Tag evaluateHtmlContent(ParseTree parseTree) {
        parseTree = parseTree.getChild(0);
        Tag tag = null;
        if (parseTree instanceof HTMLParser.DocTypeContext) {
            evaluateDocType(parseTree);
        } else if (parseTree instanceof HTMLParser.HtmlElementContext) {
            tag = evaluateHtmlElement(parseTree);
        } else {
            throw new InputMismatchException();
        }
        return tag;
    }

    /**
     * Evaluate the document type of the HTML file.
     * @param parseTree
     */
    private void evaluateDocType(ParseTree parseTree) {
        // TODO: Different document types
    }

    /**
     * Evaluate a single HTML element.
     * @param parseTree
     * @return
     */
    private Tag evaluateHtmlElement(ParseTree parseTree) {
        Tag tag = null;
        String endTagName = null;
        if (parseTree.getChildCount() == 2) {
            // TODO: Empty tags
        } else if (parseTree.getChildCount() >= 3) {
            tag = evaluateHtmlStartTag(parseTree.getChild(0));
            for (int i = 1; i < parseTree.getChildCount() - 1; i++) {
                if (parseTree.getChild(i) instanceof HTMLParser.HtmlElementContext) {
                    Tag childTag = evaluateHtmlElement(parseTree.getChild(i));
                    if (childTag != null) {
                        tag.addChildTag(childTag);
                    }
                } else if (parseTree.getChild(i) instanceof HTMLParser.PlainTextContext) {
                    tag.setContents(evaluatePlainText(parseTree.getChild(i)));
                } else {
                    throw new InputMismatchException();
                }
            }
            endTagName = evaluateHtmlEndTag(parseTree.getChild(parseTree.getChildCount() - 1)); // TODO: Use to parse broken grammar
        } else {
            throw new InputMismatchException();
        }
        return tag;
    }

    /**
     * Evaluate the starting tag of an HTML element.
     * @param parseTree
     * @return
     */
    private Tag evaluateHtmlStartTag(ParseTree parseTree) {
        String tagName = evaluateTagName(parseTree.getChild(1));
        List<Attribute> attributeList = new ArrayList<Attribute>();
        if (parseTree.getChildCount() > 3) {
            for (int i = 2; i < parseTree.getChildCount() - 1; i++) {
                attributeList.add(evaluateTagAttribute(parseTree.getChild(i)));
            }
        }
        return new Tag(tagName, attributeList);
    }

    /**
     * Evaluate the ending tag of an HTML element.
     * @param parseTree
     * @return
     */
    private String evaluateHtmlEndTag(ParseTree parseTree) {
        return evaluateTagName(parseTree.getChild(1));
    }

    /**
     * Evaluate a single attribute of an HTML tag.
     * @param parseTree
     * @return
     */
    private Attribute evaluateTagAttribute(ParseTree parseTree) {
        String attributeName = null;
        String attributeValue = null;
        for (int i = 0; i < parseTree.getChildCount(); i++) {
            if (parseTree.getChild(i) instanceof HTMLParser.AttributeNameContext) {
                attributeName = evaluateAttributeName(parseTree.getChild(i));
            } else if (parseTree.getChild(i) instanceof HTMLParser.AttributeValueContext) {
                attributeValue = evaluateAttributeValue(parseTree.getChild(i));
            }
        }
        return new Attribute(attributeName, attributeValue);
    }

    /**
     * Evaluate an HTML element attribute's name.
     * @param parseTree
     * @return
     */
    private String evaluateAttributeName(ParseTree parseTree) {
        return evaluatePlainText(parseTree);
    }

    /**
     * Evaluate an HTML element attribute's value.
     * @param parseTree
     * @return
     */
    private String evaluateAttributeValue(ParseTree parseTree) {
        return evaluatePlainText(parseTree.getChild(0));
    }

    /**
     * Evaluate an HTML element tag name.
     * @param parseTree
     * @return
     */
    private String evaluateTagName(ParseTree parseTree) {
        return evaluatePlainText(parseTree);
    }

    /**
     * Evaluate plain text.
     * @param parseTree
     * @return
     */
    private String evaluatePlainText(ParseTree parseTree) {
        return parseTree.getText();
    }

    /**
     * Return the HTML tag tree.
     * @return
     */
    public List<Tag> getTags() {
        return tags;
    }
}
