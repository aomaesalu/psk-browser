grammar HTML;

html
    :   htmlContent*
    ;

htmlContent
    :   docType
    |   htmlElement
    ;

htmlElement
    :   htmlStartTag (htmlElement*|plainText) htmlEndTag?
    ;

docType
    :   TagStartBang tagName Space tagName TagEnd
    ;

htmlStartTag
    :   TagStart tagName tagAttribute* TagEnd
    ;

tagAttribute
    :   Space+ attributeName Space* EqualSign Space* DoubleQuote attributeValue DoubleQuote
    ;

htmlEndTag
    :   TagStartSlash tagName TagEnd
    ;

tagName
    :   AlphabetLetter+
    ;

attributeName
    :   AlphabetLetter+
    ;

attributeValue
    :   plainText
    ;

plainText
    :   (AlphabetLetter|ForeignLetter|Digit|SpecialSymbol|EqualSign|DoubleQuote|Space)*
    ;

TagStart
    :   '<'
    ;

TagStartSlash
    :   '</'
    ;

TagStartBang
    :   '<!'
    ;

TagEnd
    :   '>'
    ;

AlphabetLetter
    :   [A-Za-z]
    ;

ForeignLetter
    :   [ÕÄÖÜõäööü]
    ;

Digit
    :   [0-9]
    ;

SpecialSymbol
    :   ~('<'|'>'|' '|'\t'|'='|'"'|'\n'|'\r')//[!#¤%&/()?.:,;'*+@£$€{\[\]}\\-_]
    ;

EqualSign
    :   '='
    ;

DoubleQuote
    :   '"'
    ;

Space
    :   [ \t]
    ;

Whitespace
    :   [ \t\r\n]+ -> skip
    ;