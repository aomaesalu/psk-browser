package maesalu.pskbrowser.browser;

import maesalu.pskbrowser.exceptions.InvalidIndexException;
import maesalu.pskbrowser.exceptions.InvalidIndexExceptionMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The class holding tab information.
 * @author Ants-Oskar Mäesalu
 */
public class Tab {
    private List<Site> siteList;
    private Integer currentSite;

    /**
     * Constructor with an URL parameter.
     * Used when the user opens a page in a new tab.
     * @param url
     */
    public Tab(String url) throws IOException {
        siteList = new ArrayList<Site>();
        siteList.add(new Site(url));
        currentSite = 0;
    }

    /**
     * Constructor without any arguments.
     * Used when the user opens a new tab.
     */
    public Tab() {
        siteList = new ArrayList<Site>();
        currentSite = -1;
    }

    /**
     * Switch the site to a specific index.
     * @param index
     * @throws InvalidIndexException
     */
    public void switchSite(Integer index) throws InvalidIndexException {
        if (index >= 0 && index < siteList.size()) {
            currentSite = index;
            // TODO: Reload front end
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.SITE);
        }
    }

    /**
     * Go one site back in the site history list.
     * Used when the user clicks the "Back" button.
     * @throws InvalidIndexException
     */
    public void back() throws InvalidIndexException {
        if (currentSite > 0) {
            currentSite--;
            // TODO: Show the page in the front end
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.SITE);
        }
    }

    /**
     * Go one site further in the site history list.
     * Used when the user clicks the "Forward" button.
     * @throws InvalidIndexException
     */
    public void forward() throws InvalidIndexException {
        if (currentSite < siteList.size() - 1) {
            currentSite++;
            // TODO: Show the page in the front end
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.SITE);
        }
    }

    /**
     * Add a new site to the site history list.
     * @param url
     */
    private void addSite(String url) throws IOException {
        siteList.add(new Site(url));
        currentSite++;
    }

    /**
     * Visit a new site.
     * If the currently opened site is a site opened through the history, the successive history is deleted.
     * @param url
     */
    public void visitSite(String url) throws IOException {
        if (currentSite + 1 == siteList.size()) {
            addSite(url);
        } else {
            for (int i = currentSite + 1; i < siteList.size(); i++) {
                siteList.remove(i);
            }
            addSite(url);
        }
    }

    /**
     * Reload the tab.
     */
    public void reload() throws IOException {
        siteList.get(currentSite).reload();
    }

    /**
     * Return the whole site history.
     * @return
     */
    public List<Site> getSiteList() {
        return siteList;
    }

    /**
     * Return the Site instance of a specific site in the site history.
     * @param index
     * @return
     * @throws InvalidIndexException
     */
    public Site getSite(Integer index) throws InvalidIndexException {
        if (index >= 0 && index < siteList.size()) {
            return siteList.get(index);
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.SITE);
        }
    }

    /**
     * Return the Site instance of the currently opened site.
     * @return
     * @throws InvalidIndexException
     */
    public Site getCurrentSite() throws InvalidIndexException {
        return getSite(currentSite);
    }

    /**
     * Return the index number of the current site in the site history.
     * @return
     */
    public Integer getCurrentSiteNumber() {
        return currentSite;
    }
}
