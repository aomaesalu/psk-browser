package maesalu.pskbrowser.browser;

import maesalu.pskbrowser.exceptions.InvalidIndexException;
import maesalu.pskbrowser.exceptions.InvalidIndexExceptionMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The class holding browser information.
 * @author Ants-Oskar Mäesalu
 */
public class Browser {
    private List<Tab> tabList;
    private Integer currentTab;

    /**
     * Main constructor.
     * A new tab is opened.
     */
    public Browser() {
        tabList = new ArrayList<Tab>();
        tabList.add(new Tab());
        currentTab = 0;
    }

    /**
     * Open a new tab, and set it active.
     */
    public void newTab() {
        tabList.add(new Tab());
        currentTab = tabList.size() - 1;
        // TODO: Reload front end
    }

    /**
     * Switch the tab to a specific index.
     * @param index
     * @throws InvalidIndexException
     */
    public void switchTab(Integer index) throws InvalidIndexException {
        if (index >= 0 && index < tabList.size()) {
            currentTab = index;
            // TODO: Reload front end
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.TAB);
        }
    }

    /**
     * Set the previous tab as the active tab.
     */
    public void previousTab() {
        if (currentTab > 0) {
            currentTab--;
        } else {
            currentTab = tabList.size() - 1;
        }
        // TODO: Reload front end
    }

    /**
     * Set the next tab as the active tab.
     */
    public void nextTab() {
        if (currentTab < tabList.size() - 1) {
            currentTab++;
        } else {
            currentTab = 0;
        }
        // TODO: Reload front end
    }

    /**
     * Reload a specific tab.
     * @param index
     * @throws InvalidIndexException
     */
    public void reloadTab(Integer index) throws InvalidIndexException, IOException {
        if (index >= 0 && index < tabList.size()) {
            tabList.get(index).reload();
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.TAB);
        }
    }

    /**
     * Reload all of the tabs.
     * @throws InvalidIndexException
     */
    public void reloadAllTabs() throws IOException {
        for (int i = 0; i < tabList.size(); i++) {
            tabList.get(i).reload();
        }
    }

    /**
     * Reload only the current tab.
     */
    public void reloadCurrentTab() throws IOException {
        tabList.get(currentTab).reload();
    }

    /**
     * Return the list of currently opened tabs.
     * @return
     */
    public List<Tab> getTabList() {
        return tabList;
    }

    /**
     * Return the Tab instance of a specific tab.
     * @param index
     * @return
     * @throws InvalidIndexException
     */
    public Tab getTab(Integer index) throws InvalidIndexException {
        if (index >= 0 && index < tabList.size()) {
            return tabList.get(index);
        } else {
            throw new InvalidIndexException(InvalidIndexExceptionMessage.TAB);
        }
    }

    /**
     * Return the Tab instance of the currently opened tab.
     * @return
     * @throws InvalidIndexException
     */
    public Tab getCurrentTab() throws InvalidIndexException {
        return getTab(currentTab);
    }

    /**
     * Return the index number of the current tab in the tab list.
     * @return
     */
    public Integer getCurrentTabNumber() {
        return currentTab;
    }
}
