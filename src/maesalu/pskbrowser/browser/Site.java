package maesalu.pskbrowser.browser;

import maesalu.pskbrowser.languages.html.HTMLDepicter;
import maesalu.pskbrowser.languages.html.HTMLEvaluator;

import java.io.IOException;
import java.io.InputStream;

/**
 * The class holding site information.
 * @author Ants-Oskar Mäesalu
 */
public class Site {
    private String title;
    private URL url;

    /**
     * Constructor with string type URL parameter.
     * The site is then loaded.
     * @param url
     * @throws IOException
     */
    public Site(String url) throws IOException {
        load(new URL(url));
    }

    /**
     * Load the site with the specified URL.
     * @param url
     * @throws IOException
     */
    public void load(URL url) throws IOException {
        this.url = url;
        visit();
    }

    /**
     * Reload the current site.
     * @throws IOException
     */
    public void reload() throws IOException {
        visit();
    }

    /**
     * Visit the site, and get the necessary information.
     * @throws IOException
     */
    private void visit() throws IOException {
        java.net.URL URL = new java.net.URL(getUrl());
        InputStream inputStream = URL.openStream();
        // TODO: Get site meta information
        HTMLEvaluator HTMLEvaluator = new HTMLEvaluator(inputStream);
        HTMLDepicter HTMLDepicter = new HTMLDepicter(HTMLEvaluator.getTags());
    }

    /**
     * Return the title of the site.
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Return the URL of the site.
     * @return
     */
    public String getUrl() {
        return url.getUrl();
    }
}
