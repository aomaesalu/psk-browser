package maesalu.pskbrowser.browser;

/**
 * The class holding URL information. Used for cleaning and formatting URLs.
 */
public class URL {
    private String url;

    /**
     * Constructor with string parameter specifying the URL.
     * @param url
     */
    URL(String url) {
        formatUrl(url);
    }

    /**
     * Format the URL to correspond with the standards.
     * @param url
     */
    private void formatUrl(String url) {
        // TODO: Clean and format the URL
        this.url = url;
    }

    /**
     * Return the URL.
     * @return
     */
    public String getUrl() {
        return url;
    }
}
