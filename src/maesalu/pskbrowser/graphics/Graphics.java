package maesalu.pskbrowser.graphics;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;

/**
 * Singleton class for graphics generation.
 * @author Ants-Oskar Mäesalu
 */
public class Graphics {
    private static Graphics instance;
    private Canvas canvas;
    private GraphicsContext graphicsContext;

    /**
     * Private constructor for the singleton.
     */
    private Graphics(Canvas canvas) {
        this.canvas = canvas;
        graphicsContext = canvas.getGraphicsContext2D();
    }

    /**
     * Initialize the singleton graphics class.
     */
    public static void initializeInstance(Canvas canvas) {
        if (instance == null) {
            instance = new Graphics(canvas);
        }
    }

    public void newParagraph(String text) {
        // TODO: Somewhy, the text is not outputted on the canvas... I can't figure out why.
        graphicsContext.setFont(Font.getDefault());
        graphicsContext.fillText(text, 100, 100);
        System.out.println(text);
    }

    /**
     * Return the Graphics instance.
     * @return
     */
    public static Graphics getInstance() {
        return instance;
    }
}
